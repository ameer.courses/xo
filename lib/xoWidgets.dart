// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:xo_game/xoLogic.dart';

xoLogic game = xoLogic();


class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('XO Game'),
        centerTitle: true,
        backgroundColor: Colors.blueGrey,
      ),
      body: Column(
        children: [
          for(int i = 0 ; i < 3 ; i ++)
            xoRow(i),
          Expanded(
              child: IconButton(
               icon: Icon(Icons.replay),
               onPressed: (){
                setState(() {
                  game = xoLogic();
              });
            },
          )
          )
        ],
      ),
    );
  }
}


class xoWidget extends StatefulWidget {
  final int i;
  final int j;
  String val = '';

  xoWidget(this.i, this.j);

  @override
  State<xoWidget> createState() => _xoWidgetState();
}

class _xoWidgetState extends State<xoWidget> {


  @override
  Widget build(BuildContext context) {

    return Expanded(
      child: GestureDetector(
        onTap: play,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.circular(5),
          ),
          margin: EdgeInsets.all(5),
          alignment: Alignment.center,
          child: Text(widget.val),
        ),
      ),
    );
  }

  void play(){
    //if(!game.check())
    if(game.play(widget.i, widget.j)){
      setState(() {
        widget.val = game.isX ? 'O' : 'X';
      });
      if(game.check())
        showDialog(
            context: context,
            builder: (context){
              return Dialog(
                child: Container(
                  width: 200,
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(game.res),
                      ElevatedButton(
                          onPressed: (){
                            Navigator.of(context).pop();
                            //Navigator.pop(context);
                          },
                          child: Text('OK')
                      )
                    ],
                  ),
                  alignment: Alignment.center,
                ),
              );
            }
        );

    }

  }
}



class xoRow extends StatelessWidget {
  final int i;
  xoRow(this.i);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 2,
      child: Row(
        children: [
          for(int j = 0 ; j < 3; j++)
            xoWidget(i,j),
        ],
      ),
    );
  }
}

